﻿using System;
using Conduit.ConsoleBase;
using WinSCP;
using System.IO;

namespace ExplicitFTP
{
    public abstract class Program : MainBase
    {
        private static string ftphost;
        private static int portnumber;
        private static string username;
        private static string password;
        private static string transferfolder;
        private static string ftpremotefolder;
        private static bool moveandbackupfiles;
        private static string backuppath;
        private static bool renamefilebeforeFTP;
        private static string filerenamemask;
        private static string logfilepath;
        private static bool ftpdebug;
        private static string SshHostKeyFingerprintSetting;
        static int Main()
        {
            return InitAndTryCatchWrapMainProcess(MainProcess);
        }
        protected static void MainProcess()
        {
            Initialise();
        }
        public static void Initialise()
        {

            ftphost = GetExpectedAppSetting("ftphost");
            int.TryParse(GetExpectedAppSetting("portnumber"), out portnumber);
            username = GetExpectedAppSetting("username");
            password = GetExpectedAppSetting("password");
            transferfolder = GetExpectedAppSetting("transferfolder");
            ftpremotefolder = GetExpectedAppSetting("ftpremotefolder");
            bool.TryParse(GetExpectedAppSetting("moveandbackupfiles"), out moveandbackupfiles);
            backuppath = GetExpectedAppSetting("backuppath");
            bool.TryParse(GetExpectedAppSetting("renamefilebeforeFTP"), out renamefilebeforeFTP);
            filerenamemask = GetExpectedAppSetting("filerenamemask");
            logfilepath = GetExpectedAppSetting("LogFilePath");
            bool.TryParse(GetAppSetting("ftpdebug", false).ToString(), out ftpdebug);
            SshHostKeyFingerprintSetting = GetExpectedAppSetting("SshHostKeyFingerprint");
            try
            {
                // Setup session options
                SessionOptions sessionOptions = new SessionOptions
                {
                    Protocol = Protocol.Ftp,
                    HostName = ftphost,
                    PortNumber = portnumber,
                    UserName = username,
                    Password = password,
                    FtpSecure = FtpSecure.Explicit,
                    GiveUpSecurityAndAcceptAnyTlsHostCertificate = true 
                    //FtpMode = FtpMode.Passive,
                    //SshHostKeyFingerprint = SshHostKeyFingerprintSetting
                };

                using (Session session = new Session())
                {
                    // Connect
                    if (ftpdebug)
                    {
                        session.DebugLogPath = logfilepath + DateTime.Now.ToString("yyyyMMddhhmmss") + "log.txt";
                    }
                    session.Open(sessionOptions);

                    string[] filenamestomove = Directory.GetFiles(transferfolder);


                    if (filenamestomove == null || filenamestomove.Length < 1)
                    {
                        _log.Write("No files to ftp");
                        return;
                    }



                    filenamestomove = Directory.GetFiles(transferfolder);

                    TransferOptions transferOptions = new TransferOptions();
                    transferOptions.TransferMode = TransferMode.Binary;

                    TransferOperationResult transferResult;

                    //foreach (string file in filenamestomove)
                    //{
                    try
                    {

                        //string fullbackPath = backuppath + Path.GetFileName(file);
                        _log.Write(string.Format("working with folder: {0}", transferfolder));
                        transferResult = session.PutFiles(transferfolder + "*", ftpremotefolder);
                        if (!transferResult.IsSuccess)
                        {
                            _log.Write("Failure ftp ing file");
                            _log.Write("transferResult.IsSuccess is false");
                            return;
                        }


                        // Print results
                        foreach (TransferEventArgs transfer in transferResult.Transfers)
                        {
                            _log.Write("Upload of {0} succeeded", transfer.FileName);
                        }
                        _log.Write("Ftp of file sucess");
                        if (moveandbackupfiles)
                        {
                            try
                            {

                                foreach (string file in filenamestomove)
                                {
                                    string fullbackPath = backuppath + Path.GetFileName(file);
                                    if (File.Exists(fullbackPath))
                                        File.Delete(fullbackPath);
                                    _log.Write("Moving file {0} to {1}", file, fullbackPath);
                                    File.Move(file, fullbackPath);
                                }
                            }
                            catch (Exception ex)
                            {
                                _log.Write("Error moving file but FTP was ok so deleting orignal file");
                                _log.Write(ex.Message);
                                if (ex.InnerException != null && string.IsNullOrEmpty(ex.InnerException.Message))
                                {
                                    _log.Write(ex.InnerException.Message);
                                }
                            }
                        }

                    }
                    catch (Exception ex)
                    {
                        _log.Write("Failure ftp ing file");
                        _log.Write(ex.Message);
                    }

                    session.Close();
                    session.Dispose();
                }
            }
            catch (Exception e)
            {
                _log.Write("Error encounted");
                _log.Write(e.Message);
                if (e.InnerException != null && string.IsNullOrEmpty(e.InnerException.Message))
                {
                    _log.Write(e.InnerException.Message);
                }
                _log.Write(e.StackTrace);
                // return 1;
            }

        }
    }
}
