﻿using Conduit.ConsoleBase;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using WinSCP;

namespace SFTPDownLoadFiles
{
    public abstract class Program : MainBase
    {
        private static string ftphost;
        private static int portnumber;
        private static string username;
        private static string password;
        private static string transferfolder;
        private static string ftpremotefolder;
        private static bool moveandbackupfiles;
        private static string backuppath;
        private static bool renamefilebeforeFTP;
        private static string filerenamemask;
        private static string logfilepath;
        private static bool ftpdebug;
        private static string SshHostKeyFingerprintSetting;
        static int Main()
        {
            return InitAndTryCatchWrapMainProcess(MainProcess);
        }
        protected static void MainProcess()
        {
            Initialise();
        }
        public static void Initialise()
        {

            ftphost = GetExpectedAppSetting("ftphost");
            int.TryParse(GetExpectedAppSetting("portnumber"), out portnumber);
            username = GetExpectedAppSetting("username");
            password = GetExpectedAppSetting("password");
            transferfolder = GetExpectedAppSetting("transferfolder");
            ftpremotefolder = GetExpectedAppSetting("ftpremotefolder");
            bool.TryParse(GetExpectedAppSetting("moveandbackupfiles"), out moveandbackupfiles);
            backuppath = GetExpectedAppSetting("backuppath");
            bool.TryParse(GetExpectedAppSetting("renamefilebeforeFTP"), out renamefilebeforeFTP);
            filerenamemask = GetExpectedAppSetting("filerenamemask");
            logfilepath = GetExpectedAppSetting("LogFilePath");
            bool.TryParse(GetAppSetting("ftpdebug", false).ToString(), out ftpdebug);
            SshHostKeyFingerprintSetting = GetExpectedAppSetting("SshHostKeyFingerprint");
            try
            {
                // Setup session options
                SessionOptions sessionOptions = new SessionOptions
                {
                    Protocol = Protocol.Sftp,
                    HostName = ftphost,
                    //PortNumber = portnumber,
                    UserName = username,
                    Password = password,
                    //FtpSecure = FtpSecure.Explicit,
                    //FtpMode = FtpMode.Passive,
                    SshHostKeyFingerprint = SshHostKeyFingerprintSetting
                };

                using (Session session = new Session())
                {
                    // Connect
                    if (ftpdebug)
                    {
                        session.DebugLogPath = logfilepath + DateTime.Now.ToString("yyyyMMddhhmmss") + "log.txt";
                    }
                    session.Open(sessionOptions);

                 

                    TransferOptions transferOptions = new TransferOptions();
                    transferOptions.TransferMode = TransferMode.Binary;
                   

                    TransferOperationResult transferResult;

                    //foreach (string file in filenamestomove)
                    //{
                    try
                    {


                        _log.Write(string.Format("transfering files from remote folder {0} to local folder {1}",ftpremotefolder, transferfolder));
                        transferResult = session.GetFiles(ftpremotefolder, transferfolder,false,transferOptions); 
                        
                        
                       
                        if (!transferResult.IsSuccess)
                        {
                            _log.Write("Failure ftp ing file");
                            _log.Write("transferResult.IsSuccess is false");
                            
                            return;
                        }
                        else
                        {
                            _log.Write("transferResult.IsSuccess is true");
                            _log.Write("Deleting remote files");
                            session.RemoveFiles(ftpremotefolder + "*");   
                            
                        }


                    }
                    catch (Exception ex)
                    {
                        _log.Write("Failure ftp ing file");
                        _log.Write(ex.Message);
                    }

                    session.Close();
                    session.Dispose();
                }
            }
            catch (Exception e)
            {
                _log.Write("Error encounted");
                _log.Write(e.Message);
                if (e.InnerException != null && string.IsNullOrEmpty(e.InnerException.Message))
                {
                    _log.Write(e.InnerException.Message);
                }
                _log.Write(e.StackTrace);
                // return 1;
            }

        }
    }
}
