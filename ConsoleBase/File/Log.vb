Imports System.IO
Imports System

Public Class Log
    Private _orderlog As StreamWriter

    Public Sub New(ByVal fileNamePath As String)
        _orderlog = File.AppendText(fileNamePath)
        _orderlog.AutoFlush = True
        _orderlog.WriteLine("---------------------------------------------")
        _orderlog.WriteLine("Processing commenced: " & DateTime.Now.ToString("yyyyMMdd HH:mm:ss"))
    End Sub

    Public Sub Write(ByVal message As String)
        _orderlog.WriteLine(DateTime.Now.ToString("HH:mm:ss ") & message)
    End Sub

    Public Sub Write(ByVal message As String, ByVal ParamArray args() As Object)
        _orderlog.WriteLine(DateTime.Now.ToString("HH:mm:ss ") & message, args)
    End Sub

    Public Sub Close()
        _orderlog.Close()
        _orderlog.Dispose()
    End Sub
End Class
