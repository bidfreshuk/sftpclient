Imports System.IO

Public Class Data
    Public Shared Sub Write(ByVal fileName As String, ByVal data As String)
        Using writer As StreamWriter = File.CreateText(fileName)
            writer.Write(data)
        End Using
    End Sub
End Class
