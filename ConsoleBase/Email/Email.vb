Imports System
Imports System.Net.Mail

Public Class Email
    Private _notificationToEmailAddress As String
    Private _notificationFromEmailAddress As String
    Private _notifyOnSuccess As Boolean
    Private _notificationSubject As String
    Private _remoteHost As String




    Public Property NotificationFromEmailAddress() As String
        Get
            Return _notificationFromEmailAddress
        End Get
        Set(ByVal value As String)
            _notificationFromEmailAddress = value
        End Set
    End Property

    Public Property NotificationToEmailAddress() As String
        Get
            Return _notificationToEmailAddress
        End Get
        Set(ByVal value As String)
            _notificationToEmailAddress = value
        End Set
    End Property

    Public Property NotifyOnSuccess() As Boolean
        Get
            Return _notifyOnSuccess
        End Get
        Set(ByVal value As Boolean)
            _notifyOnSuccess = value
        End Set
    End Property

    Public Property NotificationSubject() As String
        Get
            Return _notificationSubject
        End Get
        Set(ByVal value As String)
            _notificationSubject = value
        End Set
    End Property

    Public Property RemoteHost() As String
        Get
            Return _remoteHost
        End Get
        Set(ByVal value As String)
            _remoteHost = value
        End Set
    End Property

    Private _port As Integer
    Public Property Port() As Integer
        Get
            Return _port
        End Get
        Set(ByVal value As Integer)
            _port = value
        End Set
    End Property

    Public Sub SendError(ByVal ex As Exception, ByVal log As Log)
        Try
            Dim toEmailAddress As String = _notificationToEmailAddress.Replace(";", ",")
            If Not String.IsNullOrEmpty(toEmailAddress) AndAlso toEmailAddress.EndsWith(",") Then
                toEmailAddress = toEmailAddress.Substring(0, toEmailAddress.Length - 1)
            End If

            Dim msg As New MailMessage(_notificationFromEmailAddress, _notificationToEmailAddress.Replace(";", ","), _notificationSubject, ex.ToString())
            Dim mail As SmtpClient = GetSmtpClient()

            mail.Send(msg)
        Catch ex2 As Exception
            log.Write(ex.ToString())
            log.Write(ex2.ToString())
        End Try

    End Sub

    Private Function GetSmtpClient() As SmtpClient
        Return New SmtpClient(_remoteHost, _port)
    End Function

    Public Sub SendFiles(ByVal message As String, ByVal ParamArray attachmentFileName As String())
        Send(_notificationSubject, message, _notificationToEmailAddress, _notificationFromEmailAddress, attachmentFileName)
    End Sub

    'This overload was being picked up by mistake instead of one above, don't think I'm using it anymore.
    '******************   Changed from Send to SendFiles and SendMessage. Rob. 14/08/09 ***************************************
    Public Sub SendMessage(ByVal subject As String, ByVal message As String, ByVal ParamArray attachmentFileName As String())
        Send(subject, message, _notificationToEmailAddress, _notificationFromEmailAddress, attachmentFileName)
    End Sub

    Public Sub SendMessage(ByVal message As String)
        Send(_notificationSubject, message, _notificationToEmailAddress, _notificationFromEmailAddress)
    End Sub


    Public Sub Send(ByVal subject As String, ByVal message As String, ByVal ToAddress As String, ByVal FromAddress As String, ByVal ParamArray attachmentFileName As String())
        Dim toEmailAddress As String = ToAddress.Replace(";", ",")
        If Not String.IsNullOrEmpty(toEmailAddress) AndAlso toEmailAddress.EndsWith(",") Then
            toEmailAddress = toEmailAddress.Substring(0, toEmailAddress.Length - 1)
        End If

        Dim msg As New MailMessage(FromAddress, toEmailAddress, subject, message)
        'msg.IsBodyHtml = True


        Dim mail As SmtpClient = GetSmtpClient()
        If Not attachmentFileName Is Nothing Then
            For Each attFileName As String In attachmentFileName
                msg.Attachments.Add(New Attachment(attFileName))
            Next
        End If
        mail.Send(msg)
    End Sub

    Public Sub SendWithAttachment(ByVal subject As String, ByVal message As String, ByVal ToAddress As String, ByVal FromAddress As String, ByVal ParamArray attachmentFileName As String())

        Dim toEmailAddress As String = ToAddress.Replace(";", ",")
        If Not String.IsNullOrEmpty(toEmailAddress) AndAlso toEmailAddress.EndsWith(",") Then
            toEmailAddress = toEmailAddress.Substring(0, toEmailAddress.Length - 1)
        End If
        Dim msg As New MailMessage(FromAddress, toEmailAddress.Replace(";", ","), subject, message)
        ' msg.IsBodyHtml = True
        Dim mail As SmtpClient = GetSmtpClient()
        If Not attachmentFileName Is Nothing Then
            For Each attFileName As String In attachmentFileName
                msg.Attachments.Add(New Attachment(attFileName))
            Next
        End If
        mail.Send(msg)
    End Sub

    Public Sub Send(ByVal subject As String, ByVal message As String, ByVal ToAddress As String, ByVal FromAddress As String, ByVal CCAddress As String, ByVal ParamArray attachmentFileName As String())

        Dim toEmailAddress As String = ToAddress.Replace(";", ",")
        If Not String.IsNullOrEmpty(toEmailAddress) AndAlso toEmailAddress.EndsWith(",") Then
            toEmailAddress = toEmailAddress.Substring(0, toEmailAddress.Length - 1)
        End If

        Dim msg As New MailMessage(FromAddress, toEmailAddress, subject, message)
        ' msg.IsBodyHtml = True

        If Not String.IsNullOrEmpty(CCAddress) Then
            msg.CC.Add(CCAddress.Replace(";", ":"))
        End If
        Dim mail As SmtpClient = GetSmtpClient()
        If Not attachmentFileName Is Nothing Then
            For Each attFileName As String In attachmentFileName
                msg.Attachments.Add(New Attachment(attFileName))
            Next
        End If
        mail.Send(msg)
    End Sub


    Public Sub SendWithAttachment(ByVal subject As String, ByVal message As String, ByVal ToAddress As String, ByVal FromAddress As String, isHTML As Boolean, ByVal ParamArray attachmentFileName As String())

        Dim toEmailAddress As String = ToAddress.Replace(";", ",")
        If Not String.IsNullOrEmpty(toEmailAddress) AndAlso toEmailAddress.EndsWith(",") Then
            toEmailAddress = toEmailAddress.Substring(0, toEmailAddress.Length - 1)
        End If
        Dim msg As New MailMessage(FromAddress, toEmailAddress.Replace(";", ","), subject, message)
        msg.IsBodyHtml = isHTML
        Dim mail As SmtpClient = GetSmtpClient()
        If Not attachmentFileName Is Nothing Then
            For Each attFileName As String In attachmentFileName
                msg.Attachments.Add(New Attachment(attFileName))
            Next
        End If
        mail.Send(msg)
    End Sub

    Public Sub Send(ByVal subject As String, ByVal message As String, ByVal ToAddress As String, ByVal FromAddress As String, isHTML As Boolean, ByVal ParamArray attachmentFileName As String())
        Dim toEmailAddress As String = ToAddress.Replace(";", ",")
        If Not String.IsNullOrEmpty(toEmailAddress) AndAlso toEmailAddress.EndsWith(",") Then
            toEmailAddress = toEmailAddress.Substring(0, toEmailAddress.Length - 1)
        End If

        Dim msg As New MailMessage(FromAddress, toEmailAddress, subject, message)
        msg.IsBodyHtml = isHTML


        Dim mail As SmtpClient = GetSmtpClient()
        If Not attachmentFileName Is Nothing Then
            For Each attFileName As String In attachmentFileName
                msg.Attachments.Add(New Attachment(attFileName))
            Next
        End If
        mail.Send(msg)
    End Sub

End Class
